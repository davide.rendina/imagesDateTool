import re
from datetime import datetime
import piexif
import os

from os import listdir
from os.path import isfile, join


workingPath = '/Users/davide/Desktop/Photos/photosToFix'
todoPath = workingPath + "/_TODO"
donePath = workingPath + "/_DONE"

rename_files = True
execution_mode = 3 #Values: 1 (get data from filename) or 2 (fixed data for everyone), 3 (get EXIF date and update file Creation date)

fixed_date = datetime.strptime("2020-07-23-12-00-00", "%Y-%m-%d-%H-%M-%S") #Used only in execution_mode 3



def checkPathsExistance():
	if not os.path.exists(workingPath):
		print("ERROR: The path doesn't exists. This process will be killed.")
		exit()

	if not os.path.exists(todoPath):
		os.mkdir(todoPath)

	if not os.path.exists(donePath):
		os.mkdir(donePath)

def printToolInfo():
	#print("\nThe aim of this tool is to correct the date of media files (JPG, MP4, PNG), extracting it from files metadata (execution mode 3)/filename (ex.mode 1) or giving each file a fixed date. (ex.mode 3) ")
	#print("With the flag rename_files setted as true, the tool rename each file with the date, following the format YYYYmmdd_HHMMSS.")

	print("\n", "SELECTED EXECUTION MODE: ",execution_mode)
	print(" RENAME FILE FLAG: ",rename_files)

	if execution_mode == 2:
		print(" SELECTED FIXED DATE: ", fixed_date)

	print("")

def confirmConfiguration():
	confirm = input(" Do you wish to continue (y/n)? ")
	if confirm == "y":
		print("")
		return
	exit()

def getDate(date, format):
	try:
		#print(date)
		date = datetime.strptime(date, format)
		print(file," ", date)
		dateOK = True
		return date
	except ValueError as e:
        #print("Match not found for ",file)
		return None

def extractDatefromFilename(file):
	global date
	date = getDate(file[:10], '%Y-%m-%d')

	if date is None:
		date = getDate(file[:15], '%Y%m%d_%H%M%S') #20160101_010000_etc.jpg

	if date is None:
		date = getDate(file[11:30], '%Y-%m-%d-%H-%M-%S') #Quickmemo+_2016-01-01-01-00-00.png

	if date is None:
		date = getDate(file[11:-4], '%Y%m%d-%H%M%S') #Screenshot_20160101-010000.jpg

	if date is None:
		date = getDate(file[9:-4], '%Y-%m-%d-%H-%M-%S') #Capture+_2016-01-01-01-00-00.png

	if date is None:
		date = getDate(file[4:19], '%Y%m%d_%H%M%S') #IMG_20160101_230000

	if date is None:
		date = getDate(file[4:12], '%Y%m%d')

	if date is None:
		date = getDate("20"+file[:-4], '%Y-%m-%d-%H-%M-%S') #20-01-01-15-01-34.mp4

def extractDatefromMetadata(file):
	try:
		temp_date = piexif.load(workingPath+"/"+file)['Exif'][piexif.ExifIFD.DateTimeOriginal].decode('utf-8')
		date = datetime.strptime(temp_date, '%Y:%m:%d %H:%M:%S')
	except piexif._exceptions.InvalidImageDataError as e:
		date = None
	except (KeyError, ValueError) as e:
		#Significa che l'attributo non lo trova: uso la data creazione del file
		date = datetime.fromtimestamp(os.path.getmtime(mypath + "/" +file))
	return date

def modifyDate(filename, new_date):
	# Modify file info
	os.utime(filename, times=(new_date.timestamp(), new_date.timestamp()))

	# Modify EXIF if possible
	try:
		new_date = new_date.strftime("%Y:%m:%d %H:%M:%S")
		exif_dict = piexif.load(filename)
		exif_dict['0th'][piexif.ImageIFD.DateTime] = new_date
		exif_dict['Exif'][piexif.ExifIFD.DateTimeOriginal] = new_date
		exif_dict['Exif'][piexif.ExifIFD.DateTimeDigitized] = new_date
		exif_bytes = piexif.dump(exif_dict)
		piexif.insert(exif_bytes, filename)
	except piexif._exceptions.InvalidImageDataError as e:
		print(filename, " ", e)
	except ValueError as ve:
		print(filename, " ", ve)

def searchForFilename(file):
	counter = 0
	temp_file = file
	while os.path.isfile(temp_file):
		counter = counter + 1
		temp_file = os.path.splitext(file)[0] + "_" + str(counter) + os.path.splitext(file)[1]
	return temp_file





checkPathsExistance()

printToolInfo()

confirmConfiguration()


# Read all files in the folder
allFiles = [f for f in listdir(workingPath) if isfile(join(workingPath, f))]

print(" Found ",len(allFiles), " files")

# For each file, extract date if match
noneFiles = 0
for file in allFiles:
	date = None
	if execution_mode == 1:
		extractDatefromFilename(file)
	elif execution_mode == 2:
		date = fixed_date
	elif execution_mode == 3:
		date = extractDatefromMetadata(file)

	if date is not None:
		modifyDate(workingPath+"/"+file, date)
		new_file = searchForFilename(donePath+"/" + (date.strftime("%Y%m%d_%H%M%S") + os.path.splitext(file)[1] if rename_files else file))
		os.rename(workingPath+"/"+file, new_file)
	else:
		os.rename(workingPath+"/"+file, todoPath+"/"+file)
		noneFiles = noneFiles + 1

print("")
print(" File modified:     ", len(allFiles) - noneFiles)
print(" File not modified: ", noneFiles)
